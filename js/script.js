// Función scroll suave a los enlaces ancla de las secciones de la página

(function () {
  function anchorLinkHandler(e) {
    const distanceToTop = el => Math.floor(el.getBoundingClientRect().top);

    e.preventDefault();
    const targetID = this.getAttribute("href");
    const targetAnchor = document.querySelector(targetID);
    if (!targetAnchor) return;
    const originalTop = distanceToTop(targetAnchor);

    window.scrollBy({
      top: originalTop,
      left: 0,
      behavior: "smooth"
    });

    const checkIfDone = setInterval(() => {
      const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
      if (distanceToTop(targetAnchor) === 0 || atBottom) {
        targetAnchor.tabIndex = "-1";
        targetAnchor.focus();
        window.history.pushState("", "", targetID);
        clearInterval(checkIfDone);
      }
    }, 100);
  }

  const linksToAnchors = document.querySelectorAll('a[href^="#"]');

  linksToAnchors.forEach(each => (each.onclick = anchorLinkHandler));

})();

// Función Ir Arriba

(function () {
  var backTop = document.getElementsByClassName('js-cd-top')[0],
    // Scroll de la ventana del navegador (en píxels) tras el que se muestra el icono de 'Ir Arriba'
    offset = 600,
    // Scroll de la ventana del navegador (en píxels) tras el que se muestra el icono de 'Ir Arriba' atenúa su opacidad.
    // Si está habilitada la posibilidad, que se habilita o no activando o desactivando la clase css '.cd-top--fade-out' 
    offsetOpacity = 1200,
    scrollDuration = 700,
    scrolling = false;
  if (backTop) {
    // Se actualiza la visibilidad del icono al hacer scroll
    window.addEventListener("scroll", function (event) {
      if (!scrolling) {
        scrolling = true;
        (!window.requestAnimationFrame) ? setTimeout(checkBackToTop, 250): window.requestAnimationFrame(checkBackToTop);
      }
    });
    // Scroll suave hacia arriba hasta llegar a la parte superior de la ventana
    backTop.addEventListener('click', function (event) {
      event.preventDefault();
      (!window.requestAnimationFrame) ? window.scrollTo(0, 0): scrollTop(scrollDuration);
    });
  }

  function checkBackToTop() {
    var windowTop = window.scrollY || document.documentElement.scrollTop;
    (windowTop > offset) ? addClass(backTop, 'cd-top--show'): removeClass(backTop, 'cd-top--show', 'cd-top--fade-out');
    (windowTop > offsetOpacity) && addClass(backTop, 'cd-top--fade-out');
    scrolling = false;
  }

  function scrollTop(duration) {
    var start = window.scrollY || document.documentElement.scrollTop,
      currentTime = null;

    var animateScroll = function (timestamp) {
      if (!currentTime) currentTime = timestamp;
      var progress = timestamp - currentTime;
      var val = Math.max(Math.easeInOutQuad(progress, start, -start, duration), 0);
      window.scrollTo(0, val);
      if (progress < duration) {
        window.requestAnimationFrame(animateScroll);
      }
    };

    window.requestAnimationFrame(animateScroll);
  }

  Math.easeInOutQuad = function (t, b, c, d) {
    t /= d / 2;
    if (t < 1) return c / 2 * t * t + b;
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
  };

  // Manipulación de clases, necesaria si 'classList' no se soporta
  function hasClass(el, className) {
    if (el.classList) return el.classList.contains(className);
    else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
  }

  function addClass(el, className) {
    var classList = className.split(' ');
    if (el.classList) el.classList.add(classList[0]);
    else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
    if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
  }

  function removeClass(el, className) {
    var classList = className.split(' ');
    if (el.classList) el.classList.remove(classList[0]);
    else if (hasClass(el, classList[0])) {
      var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
      el.className = el.className.replace(reg, ' ');
    }
    if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
  }
})();