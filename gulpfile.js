const gulp = require('gulp');
const sass = require('gulp-dart-sass');
const autoprefixer = require ('gulp-autoprefixer');
const cssnano = require('gulp-cssnano');
const plumber = require('gulp-plumber');
const pug = require('gulp-pug');
const browserSync = require('browser-sync').create();

function estilos() {
  return gulp.src('./scss/**/*.scss')
  .pipe(sass())
  .pipe(plumber())
  .pipe(autoprefixer())
  .pipe(cssnano({
    core:true
  }))
  .pipe(gulp.dest('./css'))
  .pipe(browserSync.stream());

 }

 function vistas() {
   return gulp.src('./pug/*.pug')
   .pipe(plumber())
   .pipe(pug({
     pretty: true
   }))
   .pipe(gulp.dest('./'))
   .on('end', browserSync.reload);
 }

 function watch() {
   browserSync.init({
     server: {
       baseDir: './'
     }
   });

   gulp.watch('./scss/**/*.scss', estilos);
   gulp.watch('./pug/**/*.pug', vistas);
   gulp.watch('./*.html').on('change', browserSync.reload);
 }

 exports.estilos = estilos;
 exports.vistas = vistas;
 exports.watch = watch;
